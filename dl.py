#!/usr/bin/env python3

"""Скачивалка постов тёмы"""

from time import sleep
import json

from urllib3 import PoolManager
from bs4 import BeautifulSoup

HTTP = PoolManager()

DOMAIN = 'http://tema.livejournal.com/'


def fetch_and_parse(url):
    """Распарсить урл и вернуть soup"""
    return BeautifulSoup(
        HTTP.request('get', url).data.decode('utf-8'),
        'html.parser')


def fetch_index_links(doc):
    """Получить ссылки на посты с главной"""
    return [span.find('a')['href'] for span in doc.find_all(class_='subject')]


def get_html(doc):
    """Получить html из soup"""
    return "".join([str(t) for t in doc.contents]).strip()


def parse_tags(doc):
    """Распарсить тэги"""
    return [get_html(link) for link in doc]


def parse_post(doc):
    """Распарсить документ"""
    return {
        'title': get_html(doc.find(class_='b-singlepost-title')),
        'content': get_html(doc.find(class_='b-singlepost-body')),
        'tags': parse_tags(doc.find(class_='b-singlepost-tags-items')
                           .find_all('a'))
    }


def fetch_and_parse_post(url):
    """Скачать и распарсить один пост"""
    sleep(0.3)
    post = fetch_and_parse(url)
    return parse_post(post)


def do_magic():
    """Скачать все посты с главной тёмы"""
    doc = fetch_and_parse(DOMAIN)
    with open('posts-python.json', 'w') as posts_file:
        posts_file.write(json.dumps(
            [fetch_and_parse_post(link) for link in fetch_index_links(doc)]))


if __name__ == '__main__':
    do_magic()
