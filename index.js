#!/usr/bin/env node


const _ = require('underscore');
const async = require('async');
const cheerio = require('cheerio');
const request = require('request');
const fs = require('fs');


const DOMAIN = 'http://tema.livejournal.com/';

request(DOMAIN, (err, res, body) => {
  if (err) {
    throw err;
  }
  let $ = cheerio.load(body, { decodeEntities: false });

  const posts_links = [];
  $('span.subject a').each(function () {
    const self = $(this);
    let href, title;
    href = self.attr('href');
    title = self.html();
    posts_links.push({ href, title });
  });

  async.mapLimit(posts_links, 2, (itm, next) => {
    console.log(new Date, itm);

    request(itm.href, (err, res, body) => {
      if(err){
        return next(err);
      }
      let $ = cheerio.load(body, { decodeEntities: false });


      next(null, {
        title: $('.b-singlepost-title').html(),
        content: $('.b-singlepost-body').html(),
        tags: $('.b-singlepost-tags-items a').map(function(){
          var self = $(this);
          return self.html()
        }).toArray()
      });

    });
  }, (err, results) => {
    if(err){
      throw err;
    }

    fs.writeFileSync('posts.json', JSON.stringify(results, null, 2));

  });
});

